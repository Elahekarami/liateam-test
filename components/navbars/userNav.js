import { useState } from "react"
import { useSelector } from "react-redux"
import Link from "next/link"
import { IoIosArrowDown, IoMdArrowRoundBack } from 'react-icons/io'
import styles from "../../styles/UserNav.module.scss"

export default function UserNav(){

    const [usermenu, setUsermenu] = useState(false)
    const user = useSelector(state => state.user)

    const clickHandler = () => {
        setUsermenu(!usermenu)
    }

    return (
        <div className={styles.userNav_container}>
            <div>
                <div className={styles.user_menu_toggler}>
                    <span id="username" className={styles.username}>{`${user.userInfo.name} عزیز`}</span>
                    <div onClick={clickHandler}>
                        <span className={styles.avater}>
                            <img src={user.userInfo.avatar} alt="avatar" />
                        </span>
                        <span className="arrowDown"><IoIosArrowDown /></span>
                    </div>
                </div>
                <ul className={styles.user_menu} style={{"display": usermenu ? "block" : "none"}}>
                    <li className={styles.user_menu_item}>
                        <Link href="#">
                            <a className={styles.user_menu_link}>پروفایل</a>
                        </Link>
                    </li>
                    <li className={styles.user_menu_item}>
                        <Link href="#">
                            <a className={styles.user_menu_link}>تنظیمات</a>
                        </Link>
                    </li>
                    <li className={styles.user_menu_item}>
                        <Link href="#">
                            <a className={styles.user_menu_link}>خروج <IoMdArrowRoundBack /></a>
                        </Link>
                    </li>
                </ul>
            </div>
        </div>
    )
}