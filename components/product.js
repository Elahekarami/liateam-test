import { FaCartPlus } from "react-icons/fa"
import { useState } from "react"
import { useDispatch } from "react-redux"
import { addToCart } from "../Redux/actions/user"
import styles from "../styles/Product.module.scss"

export default function Product({data}){

    const [active, setActive] = useState(false);
    const dispatch = useDispatch()

    const clickHandler = () => {
        dispatch(addToCart(data))
        setActive(true)
    }

    return(
        <div 
            className={`${styles.product_box} ${active && styles.active}`} 
            onClick={clickHandler}
        >
            <div className={styles.product_image}>
                <img src={`http://back.dev.liateam.ir${data.medium_pic}`} alt={data.title} />
                <div 
                    className={styles.isNew}
                    style={{"display": data.isNew ? "block" : "none"}}
                >
                    جدید
                </div>
            </div>
            <div className={styles.product_details}>
                <p className={styles.product_title}>{data.title}</p>
                <p className={styles.product_volume}>{data.volume}</p>
                <div className={styles.price_box}>
                    <span className={styles.addCart}><FaCartPlus /></span>
                    <div className={styles.price}>
                        {data.price.price.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')} 
                        <span className={styles.priceText}>
                            تومان
                        </span>
                    </div>
                </div>
            </div>
        </div>
    )
}