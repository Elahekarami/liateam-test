import Link from 'next/link'
import { useState, useEffect, useRef } from "react"
import { useSelector } from "react-redux"
import { FaShoppingCart } from "react-icons/fa";
import Cart from "../../components/cart"
import styles from "../../styles/SubNav.module.scss"

const ListItem = (props) => (
    <li className={styles.navbar_item}>
        <Link href={props.href}>
            <a className={`${styles.navbar_link} ${props.active ? styles.active : ""}`}>
                {props.text}
            </a>
        </Link>
    </li>
)

export default function SubNav(){

    const [cartMenu, setCartMenu] = useState(false)
    const user = useSelector(state => state.user)
    const toggler = useRef(null)


    const cartClickHandler = (e) => {
        setCartMenu(!cartMenu)
    }

    useEffect(() => {
        
        document.addEventListener("click", (e) => {
            let container = toggler.current
            if (e.target !== container && !container.contains(e.target))
            {
                setCartMenu(false)
            }
        })
    
        return () => {
            document.removeEventListener('click', (e) => {
                if(!e.target.classList.contains("SubNav_cart_toggler__3HNY0")){
                    setCartMenu(!cartMenu)
                }
            })
        };
    }, []);

    return (
        <div className={styles.subNav}>
            <nav className={styles.nav}>
                <ul className={styles.navbar}>
                    <ListItem text="مراقبت پوست" href="/" active="true" />
                    <ListItem text="مراقبت مو" href="/" active="" />
                    <ListItem text="مراقبت بدن" href="/" active="" />
                    <ListItem text="آرایشی" href="/" active="" />
                    <ListItem text="پرفروشترین" href="/" active="" />
                    <ListItem text="جدیدترین" href="/" active="" />
                </ul>
            </nav>

            <div className={styles.cart_toggler} ref={toggler}>
                <div className={styles.cart_icon} onClick={e => cartClickHandler(e)}>
                    <FaShoppingCart />
                    <span 
                        id="cart-count" 
                        className={styles.cart_count}
                        style={{"display": user.cart.items.length ? "block" : "none"}}
                    >
                        {user.cart.items.length}
                    </span>
                </div>
                { cartMenu && <Cart /> }
            </div>
        </div>
    )
}