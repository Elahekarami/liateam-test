import { FaPhoneVolume, FaYoutube, FaTwitter, FaFacebookF, FaInstagram } from "react-icons/fa"
import {FiMail} from "react-icons/fi"
import Link from "next/link"

import styles from "../styles/Footer.module.scss"

export default function Footer() {
    return(
        <footer className={styles.footer}>
            <div className={`container ${styles.footer_top_section}`}>
                <div className={styles.contact_info}>
                    <ul className={styles.contact_list}>
                        <li className={styles.contact_list_item}>
                            <Link href="">
                                <a className={styles.contact_list_link}>مرکز پشتیبانی</a>
                            </Link>
                        </li>
                        <li className={styles.contact_list_item}>
                            <Link href="">
                                <a className={`${styles.contact_list_link} ${styles.phone}`}>
                                    <FaPhoneVolume />021 88 88 88 88
                                </a>
                            </Link>
                        </li>
                        <li className={styles.contact_list_item}>
                            <Link href="">
                                <a className={`${styles.contact_list_link} ${styles.email}`}>
                                    <FiMail />
                                    sellersupport@liateam.com
                                </a>
                            </Link>
                        </li>
                    </ul>
                </div>
                
                <div className={styles.footer_logo_container}>
                    <img className={styles.footer_logo} src="/assets/images/logo-white.png" alt="Lia Team" />
                    <h2 className={styles.footer_title}>Lia Virtual Office</h2>
                    <p className={styles.footer_subtitle}>Good Time Good News</p>
                </div>
            </div>
            <div className={`container ${styles.footer_bottom_section}`}>
                <p className={styles.footer_text}>
                هفت روز هفته ، ۲۴ ساعت شبانه‌روز پاسخگوی شما هستیم
                </p>
                <div className={styles.social_box}>
                    <Link href="">
                        <a className={styles.social_link}><FaYoutube /></a>
                    </Link>
                    <Link href="">
                        <a className={styles.social_link}><FaTwitter /></a>
                    </Link>
                    <Link href="">
                        <a className={styles.social_link}><FaFacebookF /></a>
                    </Link>
                    <Link href="">
                        <a className={styles.social_link}><FaInstagram /></a>
                    </Link>
                </div>
                <p className={styles.copyright}>
                 &copy; تمام حقوق این وب سایت متعلق به شرکت آرمان تدبیر اطلس 1398-۱۳۹7 می باشد
                </p>
            </div>
        </footer>
    )
}