import { useSelector } from "react-redux"
import CartItem from "./cartItem"
import styles from "../styles/Cart.module.scss"


export default function Cart() {

    const user = useSelector(state => state.user)

    return (
        <div className={styles.cart_container}>
            <ul className={styles.cart_list}>
                {
                    user.cart.items.length
                    ?
                    user.cart.items.map((item, index) => {
                        return <CartItem item={item} key={index} />
                    })
                    :
                    <li className={styles.cart_item}>
                        سبد خرید شما خالی است
                    </li>
                }
                {
                    user.cart.items.length
                    ?
                    <>
                        <li className={`${styles.cart_item} ${styles.total_price}`}>
                            <p>
                                جمع کل : 
                                <span>{user.cart.total_price.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')} تومان</span>
                            </p>
                            <p>
                                مبلغ قابل پرداخت : 
                                <span>{user.cart.total_price.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')} تومان</span>
                            </p>
                        </li>
                        <li className={styles.cart_item}>
                            <button className={styles.payment}>تکمیل سفارش</button>
                        </li>
                    </>
                    : 
                    ""
                }
            </ul>


        </div>
    )
}