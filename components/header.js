import TopNav from "./navbars/topNav"
import SubNav from "./navbars/subNav"

import styles from "../styles/Header.module.scss"

export default function Header() {
    return(
        <header className={`${styles.header} container`}>
            <div>
                <TopNav />
                <SubNav />
            </div>
        </header>
    )
}