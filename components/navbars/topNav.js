import Link from 'next/link'
import UserNav from "./userNav"
import styles from "../../styles/TopNav.module.scss"

const ListItem = (props) => (
    <li className={styles.navbar_item}>
        <Link href={props.href}>
            <a className={`${styles.navbar_link} ${props.active ? styles.active: ""}`}>
                {props.text}
            </a>
        </Link>
    </li>
)

export default function TopNav (){
    return (
        <div className={styles.topNav}>
            <nav className={styles.nav}>
                <ul className={styles.navbar}>
                    <li className={styles.navbar_item}>
                        <Link href="/">
                            <a className={`${styles.navbar_link} ${styles.logo}`}>
                                <img src="/assets/images/logo.png" alt="Lia Team" />
                            </a>
                        </Link>
                    </li>
                    <ListItem text="خانه" href="/" active="true" />
                    <ListItem text="فروشگاه" href="/" active="" />
                    <ListItem text="وبلاگ" href="/" active="" />
                    <ListItem text="درباره ما" href="/" active="" />
                    <ListItem text="تماس با ما" href="/" active="" />
                </ul>
            </nav>

            <UserNav />
        </div>
    )
}