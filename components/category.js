import { IoIosArrowDropleftCircle } from "react-icons/io"
import Link from "next/link"
import styles from "../styles/Category.module.scss"

export default function Category({data}){

    return(
        <div className={styles.category_box}>
            <Link href={{
                pathname: '/products',
                query: { category: data.id },
            }}>
                <a className={styles.category_link}>
                    <div className={styles.category_name}>
                        {data.name}
                        <IoIosArrowDropleftCircle />
                    </div>
                    <img className={styles.category_img} src={`http://back.dev.liateam.ir/${data.image}`} alt={data.name} />
                </a>
            </Link>
        </div>
    )
}