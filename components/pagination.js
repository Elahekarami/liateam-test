import React from 'react'
import {GrFormPrevious, GrFormNext} from "react-icons/gr"
import styles from "../styles/Pagination.module.scss"

const Pagination = ({postsPerPage, totalPosts, paginate, paginateNext, paginatePrev, current}) =>{
 
    const pageNumbers = [];

    for(let i = 1; i <= Math.ceil(totalPosts / postsPerPage); i++){
        pageNumbers.push(i);
    }

    return(
    <nav>
        <ul className={styles.pagination}>
            <li className={`${styles.page_item} ${styles.prev}`}>
                <a 
                    onClick={(e) => {
                        paginatePrev()
                        document.querySelector(".pagination-link").classList.remove(`${styles.active}`)
                        document.getElementById(`${current-1}`).classList.add(`${styles.active}`)
                    }} 
                    href="#" 
                    className={styles.page_link}
                >
                    <GrFormPrevious />
                </a> 
            </li> 
            {pageNumbers.map(number => (
                <li key={number} className={styles.page_item}>
                    <a 
                        id={number}
                        onClick={(e) => {
                            paginate(number)
                            document.querySelector(".pagination-link").classList.remove(`${styles.active}`)
                            e.target.classList.add(`${styles.active}`)
                        }} 
                        href="#" 
                        className={`${styles.page_link} ${number==1 && styles.active} pagination-link`}
                    >
                        {number}
                    </a> 
                </li>    
            ))}

            <li className={`${styles.page_item} ${styles.next}`}>
                <a 
                    onClick={(e) => {
                        paginateNext()
                        document.querySelector(".pagination-link").classList.remove(`${styles.active}`)
                        document.getElementById(`${current+1}`).classList.add(`${styles.active}`)
                    }} 
                    href="#" 
                    className={styles.page_link}
                >
                    <GrFormNext />
                </a> 
            </li> 
        </ul>
    </nav>
    )
}

export default Pagination;