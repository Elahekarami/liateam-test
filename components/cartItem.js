import { useDispatch } from "react-redux"
import { CgClose } from "react-icons/cg"
import { removeFromCart } from "../Redux/actions/user"
import { useState, useRef } from "react"
import styles from "../styles/Cart.module.scss"

export default function CartItem({item}) {

    const [count, setCount] = useState(1)
    const dispatch = useDispatch()
    const minusBtn = useRef(null)

    const increment = () => {
        if(count == 1){
            minusBtn.current.disabled = false
        }
        setCount(count + 1)
    }

    const decrement = () => {
        console.log("enter")
        setCount(count - 1)
        if(count == 1){
            minusBtn.current.disabled = true
        }
    }

    const deleteHandler = () => {
        dispatch(removeFromCart(item))
    }

    return (
        <li className={styles.cart_item}>
            <img className={styles.item_image} src={`http://back.dev.liateam.ir${item.small_pic}`} alt={item.title} />
            <div className={styles.item_details}>
                <p>{item.title}</p>
                <p className={styles.price}>
                    {item.price.price.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')} 
                    <span className={styles.priceText}>
                        تومان
                    </span>
                </p>
                <div className={styles.item_number}>
                    <button 
                        onClick={increment}
                    >
                        +
                    </button>
                    <span>{count}</span>
                    <button 
                        onClick={decrement}
                        ref={minusBtn}
                        disabled={true}
                    >
                        -
                    </button>
                </div>
            </div>
            <span className={styles.close} onClick={deleteHandler}>
                <CgClose />
            </span>
        </li>
    )
}
