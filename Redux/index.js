const redux = require("redux")
const {combineReducers, createStore} = redux
import userReducer from "./actions/user"

const reducer = combineReducers({
    user: userReducer
})

const store = createStore(reducer)

store.subscribe(() => {
    console.log(store.getState())
})

export default store