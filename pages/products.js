import { useRouter } from 'next/router'
import { useState, useEffect } from "react"
import Product from "../components/product"
import styles from '../styles/Products.module.scss'
import Pagination from '../components/pagination'

export default function Products() {
    const [products, setProducts] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [postperPage] = useState(8);
    const router = useRouter()
    const {category} = router.query
    const [loading, setLoading] = useState(true)

    useEffect(()=>{
        fetch(`http://back.dev.liateam.ir/api/rest/v1/get_product?categories=${category}`)
        .then(res => res.json())
        .then(res => {
            setProducts(res.list)
            setLoading(false)
        })
    },[]);

    const indexOfLastPost = currentPage * postperPage
    const indexOfFirstPost = indexOfLastPost - postperPage
    const currentProducts = products.slice(indexOfFirstPost, indexOfLastPost)
    
    const paginate = (pageNumber) => {
        setCurrentPage(pageNumber)
    }

    const paginatePrev = () => {
        let p = currentPage - 1
        if(p>=1){
            setCurrentPage(currentPage - 1)
        }
    }

    const paginateNext = (pageNumber) => {
        let p = products / 8;
        console.log(p)
        if(p<= pageNumber)
        {
            setCurrentPage(currentPage + 1)
        }
    }


    return (
        <div className="container page">
            <h1 className="page_title">محصولات</h1>
            <div className={styles.products}>
            {
                loading 
                ?
                "Loading"
                :
                currentProducts.map((product, index) => <Product data={product} key={index} />)
            }
            </div>
            <Pagination 
                postsPerPage={postperPage} 
                totalPosts={products.length} 
                paginate={paginate} 
                paginateNext={paginateNext} 
                paginatePrev={paginatePrev} 
                current={currentPage}
            />
        </div>
    )
}