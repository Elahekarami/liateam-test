import Layout  from '../components/layout'
import { Provider } from "react-redux"
import store from "../Redux/index"
import '../styles/globals.scss'

function MyApp({ Component, pageProps }) {
  return (
    <Provider store={store}>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </Provider>
  )
}

export default MyApp
