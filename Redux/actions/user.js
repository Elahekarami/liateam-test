export function getUser(user) {
    return {
        type: "GET_USER",
        payload: user
    }
}

export function getCart(cart) {
    return {
        type: "GET_CART",
        payload: cart
    }
}

export function addToCart(thing) {
    return {
        type: "ADD_TO_CART",
        payload: thing
    }
}

export function removeFromCart(thing) {
    return {
        type: "REMOVE_FROM_CART",
        payload: thing
    }
}

const initialState = {
    userInfo: {
        name: "رضا جباری",
        avatar: "/assets/images/avatar.png"
    },
    cart: {
        items: [],
        total_price: 0
    }
}

export default function userReducer(state = initialState, action) {
    switch(action.type){
        case "GET_USER": 
            return {user, ...state}
        case "GET_CART":
            return {...state, cart}
        case "ADD_TO_CART":
            let addedPrice = state.cart.total_price + action.payload.price.final_price;
            return {...state, cart: {items: [...state.cart.items, action.payload], total_price: addedPrice}}
        case "REMOVE_FROM_CART":
            let filteredCart = state.cart.items.filter( item => item!=action.payload)
            let reducedPrice = state.cart.total_price - action.payload.price.final_price;
            return {...state, cart: {items: filteredCart, total_price: reducedPrice}}
        default:
            return state
    }
}