import { useState, useEffect } from "react"
import Category from "../components/category"
import styles from '../styles/Home.module.scss'

export default function Home() {

  const [loading, setLoading] = useState(true)
  const [categories, setCategories] = useState()

  useEffect(()=>{
    fetch('http://back.dev.liateam.ir/api/rest/v1/get_categories')
      .then(res => res.json())
      .then(res => {
        setCategories(res)
        setLoading(false);
      })
  },[])

  return (
      <div className="container page">
        <h1 className="page_title">دسته بندی</h1>
        <div className={styles.categories}>
          {
            loading 
            ?
            "Loading"
            :
            categories.map((category, index) => <Category data={category} key={index} />)
          }
        </div>
      </div>
  )
}
